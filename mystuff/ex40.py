class Song(object):

    def __init__(self, lyrics):
        self.lyrics = lyrics

    def singMeASong(self):
        for line in self.lyrics:
            print line

    def

happyBday = Song(["Happy birthday to you",
	"I don't want to get sued",
	"So I'll stop right there"])

bullsOnParade = Song(["They rally around the family",
    "With pockets full of shells"])

goodRiddance = ["It's something unpredictable",
    "But in the end it's right",
    "I hope you had the time of your life"]

greenDaySong = Song(goodRiddance)

happyBday.singMeASong()

bullsOnParade.singMeASong()

greenDaySong.singMeASong()
