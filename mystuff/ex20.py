from sys import argv

script, inputFile = argv

def printAll(f):
    print f.read()

def rewind(f):
    f.seek(0)

def printALine(lineCount, f):
    print lineCount, f.readline()

currentFile = open(inputFile)

print "First let's pring the whole file:\n"

printAll(currentFile)

print "Now let'srewind, kind of like a tape"

rewind(currentFile)

print "Let's print three lines:"

currentLine = 1
printALine(currentLine, currentFile)

currentLine += 1
printALine(currentLine, currentFile)

currentLine += 1

printALine(currentLine, currentFile)
